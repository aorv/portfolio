var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    gulpSync  = require('gulp-sync')(gulp);

gulp.task('default', gulpSync.sync(['dist-clean', 'markup', 'styles:critical', 'images', 'fonts', 'styles', 'styles:minify', 'scripts', 'scripts:vendor', 'server' ]), function() {
  gulp.watch(config.paths.markup.all, ['markup']);
  gulp.watch(config.paths.img.src, ['images']);
  gulp.watch(config.paths.css.all, ['styles', 'styles:minify', 'styles:critical']);
  gulp.watch(config.paths.js.all, ['scripts', 'scripts:vendor']);
});
