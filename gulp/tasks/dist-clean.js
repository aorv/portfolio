var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    error     = require('../error-handler.js');

gulp.task('dist-clean', function () {
  return plugins.del([
    config.paths.markup.dest + '/**',
    config.paths.data.src,
    '!' + config.paths.markup.dest,
    '!' + config.paths.markup.dest + '/.htaccess',
    '!' + config.paths.markup.dest + '/CNAME'
  ]);
});