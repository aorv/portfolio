var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    error     = require('../error-handler.js'),
    fs        = require('fs');

gulp.task('markup', function () {
    return gulp.src(config.paths.markup.src)
    .pipe(plugins.plumber({errorHandler: error.handler}))
    .pipe(plugins.pug({
        pretty: true
    }))
    // .pipe(plugins.rename({extname: '.html'}))
    .pipe(gulp.dest(config.paths.markup.dest))
    .pipe(plugins.browserSync.stream());
});

gulp.task('markup:minify', function () {
    return gulp.src(config.paths.markup.src)
    .pipe(plugins.plumber({errorHandler: error.handler}))
    .pipe(plugins.pug())
    .pipe(gulp.dest(config.paths.markup.dest))
});
