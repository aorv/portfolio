var config    = require('../config'),
    gulp      = require('gulp'),
    plugins   = require('gulp-load-plugins')({ overridePattern: false, pattern: ['*']}),
    error     = require('../error-handler.js');

gulp.task('revision', function () {
    return gulp.src([
        config.paths.css.dest + '/*.css',
        '!' + config.paths.css.dest + '/critical.css',
        config.paths.js.dest + '/*.js',
        config.paths.js.vendorDest + '/*.js'],
    {base: 'assets'})
    .pipe(plugins.plumber({errorHandler: error.handler}))
    .pipe(plugins.rev())
    .pipe(gulp.dest(config.paths.markup.dest))
    .pipe(plugins.rev.manifest( config.paths.data.src, {
            base: './',
            merge: true
        }))
    .pipe(gulp.dest('./'))
});

gulp.task('revision:replace', ['revision'], function(){
  var manifest = gulp.src(config.paths.data.src);

  return gulp.src(config.paths.markup.dest + '/*.' + config.server.filetype)
    .pipe(plugins.revReplace({manifest: manifest}))
    .pipe(gulp.dest('./dist/'));
});