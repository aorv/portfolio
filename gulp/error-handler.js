var plugins = require('gulp-load-plugins')();

module.exports = {
  handler: function(err) {
    plugins.notify.onError({
        title:    "Gulp",
        subtitle: "Failure!",
        message:  "Error: <%= error.message %>",
        sound:    "Beep"
    })(err);

    this.emit('end');
  }
};