window.orv = window.orv || {};

orv.getYear = {
  selector: '.js-year',
  init: function() {
    return new Date().getFullYear();
  }
};

orv.scroll = {
  animClass: 'animate',
  animTrigger: '.js-anim-trigger',
  logoEl: '.logo',
  scrollPosition: function() {
    var scrollPosition = $(window).scrollTop();
    return scrollPosition
  },
  elDistanceTop: function(element) {
    var distance = $(element).offset().top;
    return distance
  },
  isOnScreen: function(el) {
    var element = el;
    var bounds = element.getBoundingClientRect();
    return bounds.top < (window.innerHeight - 150) && bounds.bottom > 0;
  },
  animateIn: function(el) {
    $(el).addClass(orv.scroll.animClass);
  },
  animateOut: function(el) {
    $(el).removeClass(orv.scroll.animClass);
  },
  init: function() {
    var setThrottle;

    $(window).scroll(function() {
      // if(setThrottle) {
      //   window.clearTimeout(setThrottle);
      // }

      // setThrottle = window.setTimeout(function() {

        $(orv.scroll.animTrigger).each(function(i, el) {
          if (orv.scroll.isOnScreen(el)) {
            orv.scroll.animateIn(this);
          } else {
            orv.scroll.animateOut(this);
          }
        });

        if (orv.scroll.scrollPosition() >= 350) {
          orv.scroll.animateIn('.header');
        } else {
          orv.scroll.animateOut('.header');
        }

      // }, 100);
    });
  }
}

orv.panel = {
  el: '.js-contact-form',
  trigger: '.js-panel-trigger',
  animClass: 'animate',
  noScrollClass: 'no-scroll',
  isOpen: function() {
    return $(orv.panel.el).hasClass(orv.panel.animClass);
  },
  close: function() {
    $('body').removeClass(orv.panel.noScrollClass);
    $(orv.panel.el).removeClass(orv.panel.animClass);
    $('.' + orv.contact.errorMsgClass).remove();
    $('.form__input').removeClass(orv.contact.errorClass);

    // Enable scrolling.
    document.ontouchmove = function (e) {
      return true;
    }
  },
  open: function() {
    $('body').addClass(orv.panel.noScrollClass);
    $(orv.panel.el).addClass(orv.panel.animClass);

    // Disable scrolling.
    document.ontouchmove = function(e) {
      e.preventDefault();
    }
  }
}

$(orv.panel.trigger).click(function(e) {
  e.preventDefault();
  if (orv.panel.isOpen()) {
    orv.panel.close();
  } else {
    orv.panel.open();
  }
});

$(document).keyup(function(e) {
   if (e.keyCode == 27) { // escape key maps to keycode `27`
      if (orv.panel.isOpen()) {
        orv.panel.close();
      } else {
        orv.panel.open();
      }
  }
});

orv.contact = {
  myEmail: '.js-my-email',
  btn: '.js-build-email',
  name: '.js-name',
  from: '.js-from',
  subject: '.js-subject',
  errorClass: 'error',
  errorMsgClass: 'error-msg',
  getVals: function() {
    var myEmail = $(orv.contact.myEmail).text(),
        name = $(orv.contact.name).val(),
        from = $(orv.contact.from).val(),
        subject = $(orv.contact.subject).val();

    return {
      myEmail: myEmail,
      name: name,
      from: from,
      subject: subject
    }
  },
  buildEmail: function() {
    $('.' + this.errorMsgClass).remove();

    if (this.getVals().name == "") {
      $(this.name).addClass(this.errorClass).after('<p class="' + this.errorMsgClass + '">Enter your name</p>');
    } else {
      $(this.name).removeClass(this.errorClass);
    }
    if (this.getVals().from == "") {
      $(this.from).addClass(this.errorClass).after('<p class="' + this.errorMsgClass + '">Enter your email</p>');;
    } else {
      $(this.from).removeClass(this.errorClass);
    }
    if (this.getVals().name.length && this.getVals().from.length >= 1 ) {
      return window.location.href = 'mailto:' + this.getVals().myEmail + '?subject=' + this.getVals().subject + '&body=Hi Andy, I\m ' + this.getVals().name + ' (' + this.getVals().from + ')%0D%0A%0D%0A';
    }
  }
}

orv.scroll.init();
$(orv.contact.btn).click(function() {
  orv.contact.buildEmail();
});
$(orv.getYear.selector).text(orv.getYear.init());

$(window).on('load', function() {
  orv.scroll.animateIn('.hero-section');
  $('.hero-container').addClass('show-bg');
  $('footer').removeClass('show-bg')
});

console.log('Thanks for taking a peak! 👀');

